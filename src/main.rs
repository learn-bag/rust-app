use std::collections::HashMap;
mod feed_reader;
use feed_reader::feed_reader::get_feeds;

fn main() {
    // Define RSS feed URLs in the variable rss_feeds
    // Use a HashMap
    // Add Hacker News and TechCrunch
    // Ensure to use String as type
    let mut rss_feeds = HashMap::from([
        ("Hacker News".to_string(), "https://news.ycombinator.com/rss".to_string()),
        ("TechCrunch".to_string(), "https://techcrunch.com/feed/".to_string()),
    ]);

    for arg in std::env::args().skip(1) {
        let mut split = arg.split(",");
        let name = split.next().unwrap();
        let url = split.next().unwrap();
        if !url.starts_with("http://") && !url.starts_with("https://") {
            panic!("Invalid URL format: {}", url);
        }
        rss_feeds.insert(name.to_string(), url.to_string());
    }

    get_feeds(&rss_feeds);
}
