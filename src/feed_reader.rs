pub mod feed_reader {
    use std::collections::HashMap;
    use std::io::Write;
    use std::thread;

    pub fn get_feeds(rss_feeds: &HashMap<String, String>) {
        // Store threads in vector
        let mut threads: Vec<thread::JoinHandle<()>> = Vec::new();

        // Loop over rss_feeds and spawn threads
        for (name, url) in rss_feeds {
            let thread_name = name.clone();
            let thread_url = url.clone();
            let thread = thread::spawn(move || {
                // Fetch URL content
                let body = reqwest::blocking::get(thread_url.clone())
                    .unwrap()
                    .text()
                    .unwrap();
                // Parse XML body
                let parsed_body = feed_rs::parser::parse(body.as_bytes()).unwrap();

                if parsed_body.feed_type == feed_rs::model::FeedType::RSS2 {
                    println!("{} form {} is an RSS2 feed", thread_name, thread_url);
                } else if parsed_body.feed_type == feed_rs::model::FeedType::Atom {
                    println!("{} form {} is an Atom feed", thread_name, thread_url);
                }

                // Print the result
                // println!("{:#?}", parsed_body);
                let now = chrono::offset::Local::now();
                let filename = format!("{}-{}.xml", thread_name, now.format("%Y-%m-%d"));
                let mut file = std::fs::File::create(filename).unwrap();
                file.write_all(body.as_bytes()).unwrap();
            });
            threads.push(thread);
        }

        // Join threads
        for thread in threads {
            thread.join().unwrap();
        }
    }
}
// cargo run -- "GitLab Blog,https://about.gitlab.com/atom.xml" "CNCF,https://www.cncf.io/feed/"
